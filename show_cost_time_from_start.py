import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime



def read_time_cost_file(file):
    ''' Read the timecost file '''
    time = []
    cost = []
    Round = lambda x, n: eval('"%.' + str(int(n)) + 'f" % ' + repr(x))
    with open(file) as f:
        for line in f:
            values = line.split()
            # Sometime the cost can be missing if it is 0
            if len(values) == 1:
                time.append(values[0])
                cost.append(0.0000)
            else:
                time.append(values[0])
                cost.append(Round(float(values[1]), 5))
    return time, cost

def time2time_from_start(time):
    time_from_start = [float(t) - float(time[0]) for t in time]
    return time_from_start

def get_stop_time(file):
    with open(file) as f:
        stop_time = f.readline()
        return stop_time.strip()

if __name__ == "__main__":
    time, cost = read_time_cost_file("timecost.txt")
    stop_time = get_stop_time("stoptime.txt")
    time_from_start = time2time_from_start(time)

    for i in range(0,len(time)):
        print(time[i], "\t", time_from_start[i], "\t", cost[i])

    stop_offset = float(stop_time) - float(time[0])
    print("Stop time: ", stop_offset)

    # plotting with pyplot 
    
    plt.plot(time_from_start, cost, label="Cost Vs Time from start")
    plt.grid()
    plt.xticks(np.arange(time_from_start[0], time_from_start[-1], 100.0))
    plt.vlines(stop_offset, cost[0], cost[-1:], color='r', label="End of jobs")
    plt.xlabel("Time from start (s)")
    plt.ylabel("Cost")
    plt.legend()
    plt.gcf().autofmt_xdate()
    plt.show()

