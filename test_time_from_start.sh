#!/bin/zsh
#
ns=test-time2
kubectl create namespace $ns

for i in $(seq 1 5)
do
  cat <<EOF | kubectl -n $ns apply -f -
apiVersion: batch/v1
kind: Job
metadata:
  name: global-$i
  labels: 
    application: kubecost
spec:
  template:
    metadata:
      labels:
        version: dev
    spec:
      containers:
      - name: c
        image: busybox
        command: ["sh", "-c", "cat /dev/zero > /dev/null"]
        resources:
          requests:
            cpu: 100m
      restartPolicy: Never
EOF
done

start_time=$(date)
echo $start_time > timecost.txt
echo "start time: $start_time"
start_time=$(date +%s)
echo "start time: $start_time"

for i in range {1..50}
do 
	date_now=$(date +%s)
	cost=$(curl -s -X GET "http://localhost:9090/model//aggregatedCostModel?window=$start_time,$(date +%s)&aggregation=namespace&namespace=$ns" | jq | grep totalCost | awk -F " " '{ print $2 } ')
	curl -s -X GET "http://localhost:9090/model//aggregatedCostModel?window=$start_time,$(date +%s)&aggregation=namespace&namespace=$ns" | jq
	echo $date_now $cost >> timecost.txt
       	sleep 20 
done
echo "Stopping jobs"
date +%s
date +%s > stoptime.txt
kubectl delete jobs --all -n $ns

for i in range {1..50}
do 
	date_now=$(date +%s)
	cost=$(curl -s -X GET "http://localhost:9090/model//aggregatedCostModel?window=$start_time,$(date +%s)&aggregation=namespace&namespace=$ns" | jq | grep totalCost | awk -F " " '{ print $2 } ')
	echo $date_now $cost >> timecost.txt
       	sleep 20 
done
