# kubecost

=======
# Kubecost

Tests on Kubecost

## Installation
To install Kubecost, run the `install_kubecost.sh` script.
The script will install kubecost a make a port-forwarding. 

## Kubecost CLI
You can use the kubecost tui to check the state of your cluster. 
Check the [kubectl-cost](https://github.com/kubecost/kubectl-cost) project webpage to test it.
You need to have `krew` installed

## Retrieve cost from API
You can retrieve the cost of your cluster from the kubecost API

For example, to retrieve the cost from namespace, you can use the following command in terminal
```bash
curl -X GET "http://localhost:9090/model/aggregatedCostModel?window=1d&aggregation=namespace" | jq
``` 
```json
{
  "code": 200,
  "status": "success",
  "data": {
    "admiralty": {
      "aggregation": "namespace",
      "environment": "admiralty",
      "cpuAllocationAverage": 0.0033644160453994154,
      "cpuCost": 0.00020547699979030573,
      "cpuEfficiency": 2,
      "efficiency": 2,
      "gpuAllocationAverage": 0,
      "gpuCost": 0,
      "ramAllocationAverage": 0.13430978722042508,
      "ramCost": 0.0010996210899310642,
      "ramEfficiency": 2,
      "pvAllocationAverage": 0,
      "pvCost": 0,
      "networkCost": 0,
      "sharedCost": 0,
      "totalCost": 0.0013050980897213699
    },
    ...

  "message": "aggregate cache hit: 1d::::::::namespace:::::false:false:true"
}
```

Cost based on label:
```bash
curl -X GET "http://localhost:9090/model//aggregatedCostModel?window=1d&aggregation=label&&aggregationSubfield=costwatch&labels=costwatch%3Dtest" | jq
```
The previous command will retrieve the cost for workloads with the label `costwatch=test`.
The key of the label (`costwatch`) has to be specified twice in the requets.


## Check Kubecost deadtime
When using the kubecost dashboard, we could see that the metrics need a lot of time to appear.
We made some test to see how much time is required bu the Kubecost API to retrieve the metrics. 
Tests were made on Kind and GCP. It seems that using the API, the metrics do not need more than 1 minute to appear be updated.

| ![Test on Kind](./costVsTime2Jobs-Kind.png) |
|:--:|
| <b>Time response Kind</b>|

| ![Test on GCP](./costVsTime2Jobs-GCP.png) |
|:--:|
| <b>Time response Kind</b>|

### Make deadtime test
In order to reproduce the tests, you can use the `test_time_from_start.sh` script to start a few cpu-intensive jobs on the cluster. Pay attention to the number of jobs you start. If you start too many jobs, the cluster might be unresponsive.  The time and cost will be logged in the `timecost.txt` file. 
After 1000 seconds, the jobs will be deleted. The delete time will be logged in the `stoptime.txt` and the monitoring of the cost will keep running and logging during 1000s. 
You can plot the cost Vs time running the `show_cost_time_from_start.py`.
```bash
python3 show_cost_time_from_start.py
```
That script will automatically plot the cost vs time evolution.

## Kubecost alerts
Kubecost provides some notifications system. You can either set it via the dashboard or via a `ConfigMap` in the `values.yaml` of the helm charts. 
It seems that in either cases, kubecost offers only daily alerts,i.e., if the daily cost of the cluster/namespace/label overruns the threshold set. 

To test the alerts, go in the `Notification` tab in the Kubecost dashboard. From there, you can set either a slack hook, an email address or both to receive the notification. 
You also need to set in the panels below the threshold for each namespace and/or cluster. Check the `recipient tick` and save. After some hours, you should receive the notification about your cluster.
Note that you can set the time at which you want to receive the notification (but always daily).

#### Notification received
In slack
```slack
* [sbo] Your 1d namespace(test-time) budget of 0.01 has been exceeded. You have spent 0.52. Details at localhost:9090/namespace.html?name=test-time
```

And as email
```
* [sbo] Your 1d namespace(test-time) budget of 0.01 has been exceeded. You have spent 0.52. Details at localhost:9090/namespace.html?name=test-time

If you'd like to unsubscribe and stop receiving these emails click here .
```
Both messages were received at 2am whereas the cluster was created around 10am the day before and the threshold was overran slightly after.
No notification time was set for this attempt.

## Reasons to drop Kubecost
During the implementation of Kubecost in the SB project, we found several problems. 
* It seems that for our test workflow, Kubecost was unable to retrieve the metrics. We suspect that this is due to the fact that the workflows we created
were not running long enought for kubecost to detect them. We could confirm that assumption by running a much longer and resource-intensive workflow, that could
get detected by kubecost in about 2-3 minutes. In order to overcome that problem, we tried to switch from the `aggregatedCostModel` API to the `/costDataModel` API which "Does not include ETL caching layer and therefore optimal for small to medium-sized clusters", according to the official documentation. However, even using that API we could not retrieve any cost of the workflow. 
By taking a look at prometheus, however, we could see that prometheus was reporting the pods of the workflow. The pods and the workflow must have somehow be lost between prometheus and Kubecost.

See below the answers from prometheus and Kubecost
| ![](./prometheus_workflow.png) |
|:--:|
| <b>Workflow in Prometheus</b>|

| ![](./prometheus_workflow.png) |
|:--:|
| <b>Workflow in Kubecost</b>|

For all theses reasons, we decided to drop Kubecost as the cost provider of the SB application, since we are not sure on how reliable it is. 

## Troubleshooting
On GCP during the test, sometimes the API would not respond to the query. 
It turns out that the jobs were to intensive, and the server could not respond to the query. By reading the logs on the deployment `kubecost-cost-analyzer` and then the container `cost-analyzer-frontend`, it appears that the server returns a code status 599. That means the server is busy with other things and cannot answer the query.
